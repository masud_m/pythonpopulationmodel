#creating the initial values
#amount of each individual
seniles = 10
adults = 10
juveniles = 10
#survival rates
sRate = 0.0
aRate = 1.0
jRate = 1.0
#birth rate
bRate = 2
#other variables
generations = 5
headerLength = 20
changed = False

#display the menu functon
def displayMenu():
    print("[1] Set Generation 0 Values")
    print("[2] Display Generation 0 Values")
    print("[3] Run Model")
    print("[4] Export")
    print("[5] Quit")
    ans = int(input("What would you like to do?"))
    #while their answer is invalid, repeat
    #while (ans != 1 and ans != 2 and ans != 3 and ans != 4 and ans != 5):
    while (ans in [1, 2, 3, 4, 5] == False):
        ans = int(input("What would you like to do?"))
    print("\n")
    if (ans == 1):
        #set the initial values
        setModelValues()
    elif (ans == 2):
        #print just the initial values
        print(convertModel(runModel(0), 0))
    elif (ans == 3):
        #if they have not changed the values yet, display a message
        if (changed == False):
            print("Currently using the default values. Press option 1 at the menu to change these values")
        #run and convert the (dictionary-based) model into a string to print
        print(convertModel(runModel(generations), generations))
    elif (ans == 4):
        if (changed == False):
            print("Currently using the default values. Press option 1 at the menu to change these values")
        #same as print but export instead
        export(convertModel(runModel(generations), generations))
    elif (ans == 5):
        #quit the script
        quit()

#a function to run the model with specified generations
def runModel(generations):
    #create a variable to hold all the data
    data = {}
    #set intital values
    data['seniles'] = [seniles]
    data['adults'] = [adults]
    data['juveniles'] = [juveniles]
    for i in range(generations):
        #get the new amount in a generation and store in temp var
        #get the new amount of individuals in each generation using their survival rate
        nSeniles = data['seniles'][i] * sRate #remember to add the current seniles too
        nSeniles += data['adults'][i] * aRate 
        nAdults = data['juveniles'][i] * jRate
        nJuveniles = data['adults'][i] * bRate

        #update the values
        data['seniles'].append(nSeniles)
        data['adults'].append(nAdults)
        data['juveniles'].append(nJuveniles)
        
    #return the resultant data
    return data

#a function to print the model - pass in the model data and amount of generations
def convertModel(model, generations):
    #print some headers
    data = createThreeColumns("Juveniles", "Adults", "Seniles", headerLength)
    data += "\n"
    #loop through the amount of generations and get specific data under each key/individual
    for i in range(generations + 1):
        #print the current iteration under the key and format into three columns
        data += createThreeColumns(str(model['juveniles'][i]), str(model['adults'][i]), str(model['seniles'][i]), headerLength)
        #nextline
        data += "\n"
    #return the data
    return data

#create a function to standardise the columns - this makes it format nicely
#take each column's data in a argument and how wide the column shoud be
def createThreeColumns(c1, c2, c3, headerLength):
   data = ""
   #use python's built-in ljust method to pad the string the specific amount needed
   data += c1.ljust(headerLength, " ")
   data += c2.ljust(headerLength, " ")
   data += c3.ljust(headerLength, " ")
   #return the string
   return data

#a function to set the initial values
def setModelValues():
    #get the initial variables (set above) and make them global
    #this ensures we change the ones above, and not create a new var in the local scope
    global seniles, adults, juveniles
    global sRate, aRate, jRate
    global bRate, generations
    global changed
    #for each of the variables, get the user input
    seniles = int(input("Initial amount of seniles: "))
    adults = int(input("Initial amount of adults: "))
    juveniles = int(input("Initial amount of juveniles: "))
    sRate = float(input("Senile survival rate: "))
    aRate = float(input("Adult survival rate: "))
    jRate = float(input("Juvenile survival rate: "))
    bRate = int(input("Birth rate: "))
    generations = int(input("Amount of generations: "))
    changed = True #the user has changed the values

#export the data with a data input
def export(data):
    #open/create a new text file
    f = open('model.txt', 'w')
    #write the data to a file
    f.write(data)
    print("Finished exporting file")


#this is the program
#display the menu and keep repeating the menu gui until they quit
while True:
    displayMenu()

    #for some reason, input without the try/catch block returns an error
    #maybe due to a windows error? i'm not sure
    #the solution is to wrap it in a try/catch block
    #when the input fails, it won't terminate the script, it will just continue
    #and so, the desired effect is achieved
    try:
        input("Press enter to continue...")
    except SyntaxError:
        pass
    
    #add some whitespace so it looks less confusing
    print("\n")

